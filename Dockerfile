FROM tomcat:latest

#Uncomment below line of code to install JAVA 8 or hihger version if JAVA is not already installed
#RUN  yum install java-11-openjdk.x86_64 -y

#Setting Java Home For MacOs Uncomment Below line
#ENV JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk-11.0.1.jdk/Contents/Home/

#Setting Java Home For Linux based OS Uncomment Below line
#ENV JAVA_HOME=/usr/lib/jvm/java-11-openjdk-11.0.11.0.9-1.el7_9.x86_64

#Setting JAVA_HOME PATH Uncomment Below line
#RUN export PATH=$PATH:$JAVA_HOME/bin

#Install Basic packages if you need to uncomment below line of code
#RUN yum install -y wget curl git 

#To Get The Following War Please build project with mvn clean install, the current war filename is HealthCheckApplication.war
ADD target/HealthCheckApplication.war /usr/local/tomcat/webapps/

#Exposing Application To Listen On Port 8080
EXPOSE 8080

#Executing Following Command To Start Tomcat Application Server 
CMD ["catalina.sh", "run"]
