This HealthCheckApplication will allow users to view the health status of the running application. The application has a home page which will allow users to welcome and proceed with the links to do health check in detail.
 
The application is deployed at tomcat server and running at port 8080. Web application is build in spring boot. H2 in memory database is used.

Steps to run application
Docker : create a docker image with the name "health-check-app" using command "docker build -t health-check-app ." and run the docker compose
 command "docker-compose up --build". From browser run "http://localhost:8080/HealthCheckApplication/healthz"
This end point will allow user to view home page of the application. This web page has two more end points exposed as 
"Click here for application Basic Health check - http://localhost:8080/HealthCheckApplication/actuator/health" and 
		"Click here for application Advanced Health check - http://localhost:8080/HealthCheckApplication/actuator/prometheus"
		The second link will provide all the required details for connection pooling.
		
		
For kubernetes : run the bash file using command "sh ./helmCreate.sh"

