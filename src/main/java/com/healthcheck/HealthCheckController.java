package com.healthcheck;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HealthCheckController {

  @RequestMapping(value = "/healthz")
  public String homePage(Model model) {
	  return "index";
  }
  
}
